Projeto Público 


Descrição do projeto:

DDL.sql --> Contém o dicionário de dados do projeto. Ex: Tabelas, indices, pk, fk, constraints, sequences e etc.

DML.sql --> Contém as manipulações dos dados do projeto. Ex: Inserts, Updates, Deletes, Merge e etc.

Packages --> Contém as funções e procedures do projeto.

Modelagem --> Contém os diagramas relacionais do projeto.
